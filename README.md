# Icov Documentation

Documentation for ADT ICOV (separated from source code repositories to have its own history)

User-manual will be automatically uploaded at:
https://icov.gitlabpages.inria.fr/documentation/icov-documentation/user-manual.pdf
